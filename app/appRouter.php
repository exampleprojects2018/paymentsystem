<?php

/* ===============================================================================================

    --- new Route('/getAddress/:postcode/:huisnummer') ---
    --- new Route('/:lidmaatschap/:profile/:code') ---
    
==================================================================================================

    We can pass arguments by using ":" before the name of the parameter.
    In our case if we put ":" before the first parameter it always indicates that 
    it needs to return the view with the name of the parameter.

    First parameter can be with or without ":" . 
    In that case that is an action, so it needs to return a function from a controller.

    We can call the ViewController directly  which will return only the desired page without any data.

    If we want to return a view with data, we need to call a controller to make a request.
    After the request we can pass the data to the View.

=================================================================================================== */

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;


$collection = new RouteCollection();

# === Afmelden View === // GET #
$collection->attachRoute(new Route('/', array(
    
    '_controller' => 'WelcomeController::welcome',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/bevestiging', array(
    
    '_controller' => 'CopernicaController::check_payment',
    'methods' => 'POST'

)));


# === GET === #
$collection->attachRoute(new Route('/:profID/:profCode', array(
    
    '_controller' => 'CopernicaController::getProfile',
    'methods' => 'GET'

)));

# === View without data === #
$collection->attachRoute(new Route('/:view', array(
    
    '_controller' => 'ViewController::showView',
    'methods' => 'GET'

)));


# ==============    ACTIONS    ========================= #


# === POST === #
$collection->attachRoute(new Route('/aanmelden', array(

    '_controller' => 'AanmeldenController::aanmelden',
    'methods' => 'POST'
)));


$router = new Router($collection);
//$router->setBasePath('/');
$route = $router->matchCurrentRequest();

# route was not provided, show error
if(!$route){

	$view = new ViewController();
	$view->showView('page404');

}
