<?php 

		//error_reporting(E_ALL); ini_set('display_errors', 'On'); 

		/* =========================================================
		 * SYS INIT
		 * ====================================================== */
		session_start();

		mb_internal_encoding("UTF-8");
		mb_http_output("UTF-8");
		ini_set('default_charset', 'UTF-8');
		header('Content-Type: text/html; charset=UTF-8');

		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_TIME, 'nl_NL');
		
		ini_set('memory_limit','252M'); 
		ini_set('max_execution_time', '60'); // Extend execution time to a minute
		
		# WEBSITE OFFLINE / ONLINE #
		define ('OFFLINE' , false);

		define ('HOST' , $_SERVER['SERVER_NAME']);
		define ('REQ_METHOD' , $_SERVER['REQUEST_METHOD']);
		define ('DOC_ROOT', 			__DIR__);
		define ('VIEW' ,DOC_ROOT .'/views');
		define ('SMARTY' ,DOC_ROOT .'/lib/smarty');

		$WEB_ROOT = HOST ==  'url_of_developing_host'  ? 'develop_root_as_subfolder'  :  '';
		define ('WEB_ROOT' , $WEB_ROOT);
		define ('ASSETS' ,WEB_ROOT .'/assets');

		set_include_path(DOC_ROOT);
		/* =========================================================
		 * Copernica CREDENTIALS
		 * ====================================================== */
		define('COP_HOST',			'secret');
    define('COP_EMAIL', 'secret');
    define('COP_ACCOUNT', 'secret');
    define('COP_PASSWORD', 'secret');
    define('COP_CHARSET', 'utf-8');

    define('COP_TOKEN', 'secret_token_created_by_the_copernica_api_system');

		$env = HOST ==  'url_of_develop'  ? 'test'  :  'prod';
		/* =========================================================
		 * EMS CREDENTIALS
		 * ====================================================== */

		if ($env == 'test') {

				# TEST COPERNICA DB 
				define('COP_DB_FACTUUR', '14');
				define('COP_DB_HERINNERING', '15');

				# TEST CREDENTIALS FOR EMS PAYMENT SYSTEM
				define('RESPONSE_URL', 'url_response');
				define('PAY_ACTION', 'url_action');
				define('SHARED_SECRET', 'secret_code');
				define('STORENAME', 'secret_storename');

		}
		else{
			
				# PROD COPERNICA DB 
		    define('COP_DB_FACTUUR', '4');
  			define('COP_DB_HERINNERING', '2');

				# PROD CREDENTIALS FOR EMS PAYMENT SYSTEM
				define('RESPONSE_URL', 'url_response');
				define('PAY_ACTION', 'url_action');
				define('SHARED_SECRET', 'secret_code');
				define('STORENAME', 'secret_storename');
		}

		/* =========================================================
		 * INCLUDE SERVICES
		 * ====================================================== */

		include 'services/PHP-Router/src/Route.php';
		include 'services/PHP-Router/src/RouteCollection.php';
		include 'services/PHP-Router/src/Router.php';

		
		include 'services/CopernicaRestAPI.php';
		include 'services/Database.php';

		


		/* =========================================================
		 * 		INCLUDE CLASSES
		 * ====================================================== */
		include ('services/functions.inc.php');

		/* =========================================================
		 * 		INCLUDE SMARTY
		 * ====================================================== */
		require('lib/smarty/Smarty.class.php');
		# include the router
		require 'app/appRouter.php';