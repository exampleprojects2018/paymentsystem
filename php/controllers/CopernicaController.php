<?php

class CopernicaController {

		public function __construct() {

			$this->cop = new CopernicaModel();
			$this->View = new ViewController();
		}


		public function getProfile($profID, $profCode){

			$aProfile = $this->cop->getProfile($profID, $profCode);
		
			# id the customer has already payed,don't show the invoce	
			if($aProfile['fields']['status'] == 'accepted'){
					return $this->View->showBedankt('payed_already');
			}

			$aSubProfile = $this->cop->getSubProfile($profID);
			$fase = array_key_exists('fase', $aProfile['fields']) ? $aProfile['fields']['fase'] : "";

			$chargetotal = array_key_exists('tot_waarde', $aProfile['fields']) ? $aProfile['fields']['tot_waarde'] : $aProfile['fields']['inclwaarde'];


			$currency = 978;

			$chargetotal = str_replace(",",".",$chargetotal);

			$hash = $this->getHash($chargetotal, $currency);


			if(strlen($aProfile['fields']['lidnum']) == 8){
					$lidNr = "0" . $aProfile['fields']['lidnum'];
			}

			elseif(strlen($aProfile['fields']['lidnum']) == 10){
					$lidNr = substr($aProfile['fields']['lidnum'], 1);
			}
			else{
					$lidNr = $aProfile['fields']['lidnum'];
			}

			$OID = $lidNr. "I000" .rand(100,999);
			$totalBedrag = array_key_exists('inclwaarde' , $aProfile['fields']) ? $aProfile['fields']['inclwaarde'] : $aProfile['fields']['tot_waarde'];
			$aProfile['fields']['ID'] = $aProfile['ID'];
			
			$aData = [
				'Profile' => $aProfile['fields'],
				'SubProfile' => $aSubProfile["data"],
				'Betalen' => [

					'txndatetime' => date("Y:m:d-H:i:s"), 
					'hash' => $hash,
					'oid' => $OID ,
					'totalBedrag' => $totalBedrag
				]
			];

			$_SESSION['chargetotal'] = $totalBedrag;
			$_SESSION['currency'] = $currency;
			$_SESSION['txndatetime'] = $aData['Betalen']['txndatetime'];

			$this->changeStatus($profID, 'landed');

			switch ($fase) {
	
				case "100" :
					return $this->View->showView('eersteherinnering',$aData);
				break;

				case "200" :
					return $this->View->showView('tweedeherinnering',$aData);
				break;

				case "300" :
					return $this->View->showView('eersteaanmaning',$aData);
				break;

				case "400" :
					return $this->View->showView('laatsteaanmaning',$aData);
				break;
				      
				default:	
					return $this->View->showView('factuur',$aData);
				break;

			}
			
		}

		public function getHash( $chargetotal, $currency){

			$stringToHash = STORENAME . date("Y:m:d-H:i:s") . $chargetotal . $currency . SHARED_SECRET;
			$ascii = bin2hex($stringToHash);

			return hash("sha256",$ascii);
		}


		public function changeStatus($profID, $status){

				$aData = array(
	    		
			    "fields" => array(
			        'status' =>  $status,
			     )
				);

				return $this->cop->updateProfile($profID, $aData);
		}

		public function createProfile($profID, $profCode){


				$aData = array(
				    'fase' =>  '000',
				    'lidnum'  =>  '12121212',
				    'adres1'     =>  'Karel Doormanweg 41'
				);

				return $this->cop->createProfile($aData);
		}

		public function check_payment() {

			# === CHECK THE HASH === #
			$stringToHash = SHARED_SECRET . $_POST["approval_code"] . $_POST['chargetotal'] . $_POST['currency'] . $_POST['txndatetime'] . STORENAME;

			$ascii = bin2hex($stringToHash);
			$hash_check = hash("sha256",$ascii);

			$response['status'] =  explode(":", $_POST["approval_code"])[0];

			if($_POST['response_hash'] == $hash_check){

					# === CHECK THE STATUS === #
					switch ($response['status']) {

						// if the payment was already made and the client wants to repay
						case 'Y':

								$status = "accepted";
							break;

						case 'N':

							if(strpos($_POST['approval_code'],"Cancelled") > 0){

								$status = "cancel";
							}
							elseif(strpos($_POST['approval_code'],"already exists")){

								$status = 'payed_already';
							}
							else{

								$status = "failed";
							}

							break;
						
						default:

								$status = "";
							break;
					}

					$profID = $_POST['profileID'];

					$this->changeStatus($profID, $status);


			}
			else{

				$status = "failed";
				$this->logError($_POST);
					
			}

			return $this->View->showBedankt($status);
		}

		public function logError($aResponse) {

				$timestamp =  date("Y-m-d h:i:s");

				$log = "--- " . $timestamp . " --- \n";
				
				foreach ($aResponse as $key => $value) {

					$log .= '"' . $key .'" => ' . $value ."\n";
				}

				$log .= "\n";

				file_put_contents("app/error_log.txt", $log, FILE_APPEND | LOCK_EX);
				
		}

}