<?php



class ViewController {

		public function __construct() {

				$this->smarty = new Smarty();
				$this->smarty->setTemplateDir(VIEW);
				$this->smarty->setCompileDir(SMARTY . '/templates_c');
				$this->smarty->setCacheDir(SMARTY. '/cache');			


		}



		public function showView($view, $aData = null) {

				include 'views/partials/header.php';


				if(!array_key_exists('Profile', $aData)){
						$aData['Profile'] = [];
				}


				if ($view != 'bedankt') {
					# show the website only for admin if online
					if(OFFLINE  && (!in_array("tvandermade@consumentenbond.nl", $aData['Profile']) && !in_array("Tvandermade@consumentenbond.nl", $aData['Profile'])))
					{

						$this->smarty->display('offline.tpl');
					}
					else{

						$this->smarty->assign('aData', $aData);
						try {
							$this->smarty->display($view .'.tpl');
						}
						catch(Exception $e){
							$this->smarty->display('page404.tpl');
							//die($e);
						}
					}
				}
				else{

						$this->smarty->assign('aData', $aData);
						try {
							$this->smarty->display($view .'.tpl');
						}
						catch(Exception $e){
							$this->smarty->display('page404.tpl');
							//die($e);
						}
					}
				
				
			
				include 'views/partials/footer.php';

		}

		public function showBedankt($status){

			switch ($status) {

				case 'payed_already':

					$header = "Betaling reeds gedaan";
					$color = "green";
					$text = "U heeft reeds betaald en kunt dit venster sluiten.";
					break;

				case 'accepted':

					$header = "Betaling gelukt";
					$color = "green";
					$text = "Uw betaling is ontvangen. Deze zal zo spoedig mogelijk worden verwerkt.";

					break;

				case 'cancel':

					$header = "Betaling geannuleerd";
					$color = "red";
					$text = "U heeft uw betaling geannuleerd.
										<br><br>
										Als u nog vragen heeft neem dan contact op met de afdeling Service &amp; Advies van de
										Consumentenbond, via https://www.consumentenbond.nl/service/contactformulier. Zij zijn ook
										telefonisch bereikbaar <br>op 070 – 445 45 45 tijdens werkdagen tussen 08.30 en 18.00 uur.";
					break;


				case 'failed':

					$header = "Betaling mislukt";
					$text = "De betaling is helaas mislukt. Probeer het alstublieft nogmaals. Mocht deze fout terugkeren neem dan contact op met de afdeling Service & Advies van de Consumentenbond, via https://www.consumentenbond.nl/service/contactformulier. <br>Zij zijn ook telefonisch bereikbaar op 070 - 445 45 45 tijdens werkdagen tussen 08.30 en 18.00 uur.";
					$color = "red";
					break;

				
				default:
					# code...
					break;
			}

			$aData = [
				'header' => $header,
				'color' => $color,
				'text' => $text,
			];

			$this->showView('bedankt', $aData);

		}





}