<?php


class CopernicaModel {

		public $RestApi;

		public $DB_Factuur = COP_DB_FACTUUR;
		public $DB_Herinnering = COP_DB_HERINNERING;

		public function __construct(){			

			try {

    			$this->RestApi = new CopernicaRestApi(COP_TOKEN);
			}

			catch (Exception $e) {
					print_r($e);
      }

		}

		public function getProfile($profId, $profCode) {

				try {
						$aProfile = $this->RestApi->get("profile/". $profId );

				} catch (Exception $ex) {
						die($ex);
				}

				return $aProfile;
    }

    public function createProfile($aData) {

	    	try {
						$result = $this->RestApi->post("database/".$this->DB_Factuur."/profiles", $aData);

				} catch (Exception $ex) {
						die($ex);
				}


				# id of the previously created profile
				return $result;	
    }


    public function updateProfile($profId, $aData) {

	    	try {
						$result = $this->RestApi->put("profile/{$profId}", $aData);

				} catch (Exception $ex) {
						die($ex);
				}

				# boolean
				return $result;	
    }

    public function getSubProfile($profID){

    	try {
						$result = $this->RestApi->get("profile/". $profID. "/subprofiles/Order");

			} catch (Exception $ex) {
					die($ex);
			}

			return $result;	
    }

	}