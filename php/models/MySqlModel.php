<?php


class MySqlModel {

		public function __construct ($DB) {

				$this->Db = new Database($DB);
		}


		public function retrieve($req) {

				$sSelect = implode(', ', array_values($req['select']));	

	  		$sql = "SELECT " . $sSelect. " FROM " . $req['table'] ;


		  	# prepare join requirements
				if(isset($req['join'])){

						$sJoin = $this->setJoin($req['join'], $req['table']);
				}

				# prepare where requirements
				if (isset($req['where'])) {

						$sWhere = $this->setWhere($req['where']);	
						$aBind = $this->setBinding ($req['where']);	
				}

				# attach JOIN clause
		  	if(isset($sJoin)){
		  			$sql .= $sJoin;
		  	}
		  	# attach WHERE clause
		  	if(isset($sWhere)){
		  			$sql .= $sWhere;
		  	}
		  	# attach AND clause
		  	if(isset($sAndClause)){
		  			$sql .= $sAndClause;
		  	}
		  	# attach ORDER clause
		  	if(isset($req['order'])){

		  			$sql .= " ORDER BY " . $req['order'];
		  	}
		  	# attach LIMIT clause
		  	if(isset($req['limit'])){

		  			$sql .= " LIMIT " . $req['limit'];
		  	}
		  	
		    $res = $this->Db->fetchArray($sql, $aBind);

		    if(count($res) > 0){
		    	return $res;
		    }
		    else {
		    	return false;
		    }
		}
	


		public function insert($aRequirements) {

				try{
						# Build insert columns and prepared values
						$sFields 	= implode(', ', array_keys($aRequirements['insert']));	
						$sValues 	= ':' . implode(', :', array_keys($aRequirements['insert']));

						$aBinding = $this->setBinding ($aRequirements['insert']);
						# Prepare query
						$sql = "INSERT INTO " . $aRequirements['table'] . " (" . $sFields . ") VALUES (" . $sValues . ")";
				
						# Execute query
						if($this->Db->execute($sql, $aBinding)) {

							return true;

						}
			}
			catch(Exception $e){

				 		die($e->getMessage());
				 // if ($e->getCode() == 23000) {
				 // 		$this->error = "The email is already exists";
				 // }
			}

			
		}

		public function update($requirements) {

				$sWhere = $this->setWhere($requirements['where']);	
				# Build set columns and prepared values

				$sSetClause = rtrim($this->setUpdateFields($requirements['set']), ', ');

				array_push($requirements['set'], $requirements['where']);
				$aBinding = $this->setBinding ($requirements['set']);

				$sql = "UPDATE " . $requirements['table'] . " SET ". $sSetClause . $sWhere;

				# Execute query
				if($this->Db->execute($sql, $aBinding)) {

						return true;
				}
			
		}

		public function delete($requirements) {

				$sWhere = $this->setWhere($requirements['where']);	
    		$aBinding = $this->setBinding ($requirements['where']);
				
    		$sql = "DELETE FROM ". $requirements['table'] . $sWhere;

    		# Execute query
				if($this->Db->execute($sql, $aBinding)) {

						return true;
				}	
		}


		public function setWhere($aWhere) {

				$aBind = [];
				$aRequirements = [];

				foreach ($aWhere as $sWhere) {
					
						$aData = explode("|", $sWhere);

						# create binding array
						$aBind[':'.$aData[0]] = $aData[2];
						# prepare array for where clause
						$aReq = $aData[0] . '=:' .$aData[0];
						array_push($aRequirements, $aReq);
				}

				$reqCount = count($aRequirements);

				$sWhere = " WHERE " . $aRequirements[0];		
				
				if($reqCount > 1){

						for ($i=1; $i < $reqCount; $i++) { 

							$sWhere .= " AND " . $aRequirements[$i];
						}
				}


				return $sWhere;	
		}


		public function setBinding ($aData) {

				$aBind = [];
	
				foreach ($aData as $key => $data) {

						if(is_numeric($key)){

							if(is_array($data)) {

									foreach ($data as $value) {
										
										$aTemp = explode("|", $value);
										# create binding array
										$aBind[':'.$aTemp[0]] = $aTemp[2];
									
									}
							}
							else{

									$aTemp = explode("|", $data);
									# create binding array
									$aBind[':'.$aTemp[0]] = $aTemp[2];
							}

						}	
						else {
							$aBind[':'.$key] = $data;

						}				
				}


				return $aBind;
		} 


		public function setJoin($aJoin, $sTable) {

				foreach ($aJoin as $join) {
						
						$aData = explode("|", $join);
				}

					$table1 = $sTable;
					$table2 = $aData[1];

					$sJoin = ' ' . strtoupper($aData[0]).' ' . $table2 . " ON " . $table1.'.'.$aData[2] .'='. $table2.'.'.$aData[3];	

					return $sJoin;	
		}

		public function setUpdateFields($input) {


			$sSetClause = '';

				foreach ($input as $key => $value) {
						
						$sSetClause .= $key . '=:' . $key .', ';
				}

			return $sSetClause;
		}


}

