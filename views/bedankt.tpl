<div class="bedankt">

	<img src="{$smarty.const.ASSETS}/consumentenbond_logo.png" alt="Consumentenbond" width="350">

	<div class="{$aData.color} header">
		<p>{$aData.header}</p>
	</div>
	<div class="bedanktText">
		<p>Geachte heer/mevrouw,</p>

		<p>{$aData.text}</p>
	</div>
	<div class="bedanktFooter">
		<p>
			<a href="https://www.consumentenbond.nl/service/contactformulier">Contact</a>&nbsp;&nbsp;&nbsp;
			<a href="https://www.consumentenbond.nl/over/voorwaarden/Disclaimer/">Disclaimer</a>
		</p>
	</div>
</div>
<div class="col-md-12 bedanktCopyright">
    © Consumentenbond {$smarty.now|date_format:"Y"}
</div>