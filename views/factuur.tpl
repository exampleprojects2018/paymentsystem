<form id="betalen" method="post" action="{$smarty.const.PAY_ACTION}">
	{include file='partials/conf.betalen.tpl'}
</form>

<div class="header col-lg-12">

	<div class="col-md-6 col-sm-6">
		<a class="" href="https://www.consumentenbond.nl" target="_blank" >
			<img src="{$smarty.const.ASSETS}/consumentenbond_logo.png" alt="Consumentenbond">
		</a>
	</div>
	<div class="col-md-6 col-sm-6">
        Uw relatienummer: {$aData.Profile.lidnum}
        <br>
       	Factuurnummer: {$aData.Profile.fact_num}
       	<br>
    		Datum:&nbsp;{$smarty.now|date_format:"%e %B %Y"}
  </div>

</div>

<div class="body-top col-md-12">
	<div class="col-md-12">
		<h3>Factuur Consumentenbond</h3>	
	</div>
	<div class="col-md-6">
      <p style="color: #333333;">

					{if $aData.Profile.naam_zakelijk!=""}{$aData.Profile.naam_zakelijk}
					{/if} {if $aData.Profile.naam2_zakelijk!=""}{$aData.Profile.naam2_zakelijk}
					<br>
					{/if} {if $aData.Profile.adr_3|trim!=""}{$aData.Profile.adr_3}
					<br>
					{/if} {if $aData.Profile.postbus!=""} Postbus {$aData.Profile.postbus} {else}{$aData.Profile.straat1} {$aData.Profile.hnr} {$aData.Profile.hnr_toev}{/if} 
					<br>
					{$aData.Profile.pc_postbus}{$aData.Profile.postcode} {$aData.Profile.plaats}
					<br>
					{$aData.Profile.landcode}
      </p>                                           
	</div>
	<div class="col-md-6">
    <p style="color: #333333;">

    	{if $aData.Profile.w_naam!="" || $aData.Profile.w_naam_zakelijk!=""} 
    	<u>Afleveradres:</u><br>
			{if $aData.Profile.w_naam_zakelijk!=""}{$aData.Profile.w_naam_zakelijk}
			{/if} {if $aData.Profile.w_naam!=""}{$aData.Profile.w_voorl} {$aData.Profile.w_tuss} {$aData.Profile.w_naam}
			<br>
			{/if} {$aData.Profile.w_straat1} {$aData.Profile.w_hnr} {$aData.Profile.w_hnr_toev}
			<br>
			{$aData.Profile.w_postcode} {$aData.Profile.w_plaats}
			<br>
			{$aData.Profile.w_landcode}
			{/if}

    </p>    
	</div>
	<div class="col-md-12">

		<span style="font-size: 16px; font-family: Arial, Helvetica, 'Helvetica Neue', sans-serif; color: #333333; line-height: 24px;">
        <p>
        <br>
        <br>
        {if $aData.Profile.aanhef|trim!=""}{$aData.Profile.aanhef}{else}Geachte heer, mevrouw{/if},
        <br>
        <br>
        Hierbij ontvangt u een factuur voor uw abonnement bij de Consumentenbond.</p>
        <p>Wij verzoeken u vriendelijk het onderstaand bedrag voor {$aData.Profile.betaaldatum} <br>aan ons over te maken op IBAN-nummer <strong>NL27 ABNA 045 22 22 222</strong> <br>met betalingskenmerk {$aData.Profile.ocr_kenmer|substr:0:4} {$aData.Profile.ocr_kenmer|substr:4:4} {$aData.Profile.ocr_kenmer|substr:8:4} {$aData.Profile.ocr_kenmer|substr:12:4}.</p>
        <p> U kunt gemakkelijk online betalen via iDEAL: </p>
    </span>    
	</div>
	<div class="col-md-12 iDealBtn">
    <img src="{$smarty.const.ASSETS}/iDeal.png" alt="iDeal" id="iDeal">
  </div>

  <div class="col-md-12">
  	<p>
  		Voor iDEAL betalingen maakt de Consumentenbond sinds 31 maart 2018 gebruik van de betaalservice EMS, onderdeel van de ABN AMRO. Het IBAN wat u ziet bij de betaling staat ten name van St. Derdegelden EMS inzake Consumentenbond.
  	</p>
  </div>
</div>

<div class="description col-md-12">
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
      <tr>
          <td >
              Omschrijving
          </td>
          <td >
              Aantal
          </td>
          <td >
              Bedrag
          </td>
      </tr>

			{foreach from=$aData.SubProfile item=Orders}
			<tr>
				<td  valign="top">
				{$Orders.fields.art_oms}{if $aData.Profile.setlement_from<>""}
				Periode {$aData.Profile.setlement_from} - {$aData.Profile.setlement_to}
				{/if}
				</td>
				<td valign="top">{$Orders.fields.aantal}</td>
				<td valign="top">&euro;{$aData.Profile.tot_waarde|replace:'.':','}</td>
			</tr>
			{/foreach}

      <tr class="totalBetalen">
      	<td valign="top" class="btwText">
      		<div class="smallText">(Totaalbedrag BTW is &euro; {$aData.Profile.btw_bedrag})</div>
      	</td>
        <td colspan="1" valign="top" >
            Totaal te betalen 
        </td>
        <td  align="right" valign="top">
           &euro;{$aData.Profile.tot_waarde|replace:'.':','} 
        </td>
      </tr>

      <tr>
          <td colspan="3">
              <br><br>
              De Consumentenbond is een vereniging maar toch voor een deel BTW-plichtig. Hierdoor kan het BTW-bedrag afwijken van het normale tarief.
          </td>
      </tr>
    </tbody>
  </table>
</div>

<div class="body-bottom col-md-12">
	<div>
      Kijk op onze website voor een compleet overzicht van alle <a href="https://www.consumentenbond.nl/binaries/content/assets/cbhippowebsite/nieuws/consumentenbond_tarieven_online_2017.pdf"  target="_blank">abonnementstarieven</a> vanaf 1 januari 2017.
  </div>

  <div>Heeft u nog vragen?</div> 
   <p>	
	  	Mocht u vragen of opmerkingen hebben, neem dan contact op met de afdeling Service & Advies van de Consumentenbond, via <a href="https://www.consumentenbond.nl/service/contactformulier">https://www.consumentenbond.nl/service/contactformulier</a>. Zij zijn ook telefonisch bereikbaar op 070 - 445 45 45 tijdens werkdagen tussen 08.30 en 18.00 uur. Of kijk eens op onze website voor <a class="" style="font-family: Arial, Helvetica, 'Helvetica Neue', sans-serif; font-size: 16px; font-weight: normal; font-style: normal; text-decoration: underline; color: #0072bc;" title="" href="https://www.consumentenbond.nl/service/betalen" target="_blank" data-conversionlink="true">meer informatie over betalen</a>.
  </p>

</div>
<div class="footer col-md-12">

	<div class="footer-links col-md-12">
	
	  <a href="http://consumentenbond.nl/over-ons/voorwaarden-en-privacy/privacy/privacyverklaring/#anchor_3" target="_blank">
	  	Privacy
	  </a>
		<span class="mHide">&nbsp;&nbsp;-&nbsp;&nbsp;</span>

	  <a href="http://consumentenbond.nl/over/voorwaarden/Disclaimer/" target="_blank">
	  	Disclaimer
	  </a>
	  <span class="mHide">&nbsp;&nbsp;-&nbsp;&nbsp;</span>

	  <span >Uw&nbsp;e-mailadres: {$aData.Profile.email} <a href=""></a></span>
	</div>

	<div class="col-md-12">
		U ontvangt dit bericht omdat u een lidmaatschap heeft bij&nbsp;de Consumentenbond.&nbsp;<br>

    © Consumentenbond {$smarty.now|date_format:"Y"}
    <br>
    BTW-nummer:&nbsp;NL0028.77.326.B.01 &nbsp; &nbsp; &nbsp; &nbsp;BIC code: ABNANL2A &nbsp; &nbsp; &nbsp; &nbsp;KVK: 40 40 94 95
	</div>
</div>

