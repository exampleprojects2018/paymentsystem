<form id="betalen" method="post" action="{$smarty.const.PAY_ACTION}">
	{include file='partials/conf.betalen.tpl'}
</form>
<!-- {$aData|var_dump} -->
<div class="header col-lg-12">

	<div class="col-md-6 col-sm-6">
		<a class="" href="https://www.consumentenbond.nl" target="_blank" >
			<img src="{$smarty.const.ASSETS}/consumentenbond_logo.png" alt="Consumentenbond">
		</a>
	</div>
	<div class="col-md-6 col-sm-6">
        Uw relatienummer: {$aData.Profile.lidnum}
        <br>
    		Datum:&nbsp;{$smarty.now|date_format:"%e %B %Y"}
  </div>

</div>

<div class="body-top col-md-12">
	<div class="col-md-12">
		<h3>Laatste aanmaning factuur Consumentenbond</h3>	
	</div>
	<div class="col-md-12">
      <p style="color: #333333;">

					{if $aData.Profile.adres1}{$aData.Profile.adres1}{/if}</br>
					{if $aData.Profile.adres2}{$aData.Profile.adres2}{/if}</br>
					{if $aData.Profile.adres3}{$aData.Profile.adres3}{/if}</br>
					{if $aData.Profile.adres4}{$aData.Profile.adres4}{/if}</br>
					{if $aData.Profile.adres5}{$aData.Profile.adres5}{/if}</br>
      </p>                                           
	</div>

	<div class="col-md-12">

		<span style="font-size: 16px; font-family: Arial, Helvetica, 'Helvetica Neue', sans-serif; color: #333333; line-height: 24px;">
        <p>
        <br>
        <br>
        Geachte heer, mevrouw,
        <br>
        <br>
        U heeft inmiddels 2 herinneringen en een aanmaning ontvangen. Desondanks hebben wij het door u verschuldigde bedrag nog niet ontvangen. Inmiddels is het bedrag verhoogd met € 7,00 aanmaankosten.
				<br><br>
				Hierbij sommeren wij u het verschuldigde bedrag binnen 14 dagen te voldoen. Deze termijn begint 2 dagen na dagtekening van deze aanmaning. 
				Mocht u alsnog in gebreke blijven, dan wordt de vordering overgedragen aan het incassobureau. 
				Wij maken u erop attent dat dit incassokosten met zich meebrengt, welke op u verhaald zullen worden. 
				Het totaalbedrag aan aanmaan- en incassokosten bedraagt € 40,00.
				<br><br>
				Wij verzoeken u nadrukkelijk het verschuldigde bedrag binnen gestelde 
				<br>
				termijn te voldoen op IBAN-nummer<strong> NL27 ABNA 045 22 22 222 </strong>
				<br>
				met betalingskenmerk {$aData.Profile.km1} {$aData.Profile.km2}.
				</p>
        <p> U kunt gemakkelijk online betalen via iDEAL: </p>
    </span>    
	</div>
	<div class="col-md-12 iDealBtn">
    <img src="{$smarty.const.ASSETS}/iDeal.png" alt="iDeal" id="iDeal">
  </div>

  <div class="col-md-12">
  	<p>
  		Voor iDEAL betalingen maakt de Consumentenbond sinds 31 maart 2018 gebruik van de betaalservice EMS, onderdeel van de ABN AMRO. Het IBAN wat u ziet bij de betaling staat ten name van St. Derdegelden EMS inzake Consumentenbond.
  	</p>
  </div>
  
</div>

<div class="description col-md-12">
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
      <tr>
          <td >
              Omschrijving
          </td>
          <td >
              Aantal
          </td>
          <td >
              Bedrag
          </td>
      </tr>

			{if $aData.Profile.artoms1 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms1}</td>
					<td>{$aData.Profile.aantal1}</td>
					<td>&euro; {$aData.Profile.bedrg1}</td>
				</tr>
			{/if}

			{if $aData.Profile.artoms2 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms2}</td>
					<td>{$aData.Profile.aantal2}</td>
					<td>&euro; {$aData.Profile.bedrg2}</td>
				</tr>
			{/if}

			{if $aData.Profile.artoms3 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms3}</td>
					<td>{$aData.Profile.aantal3}</td>
					<td>&euro; {$aData.Profile.bedrg3}</td>
				</tr>
			{/if}

			{if $aData.Profile.artoms4 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms4}</td>
					<td>{$aData.Profile.aantal4}</td>
					<td>&euro; {$aData.Profile.bedrg4}</td>
				</tr>
			{/if}

			{if $aData.Profile.artoms5 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms5}</td>
					<td>{$aData.Profile.aantal5}</td>
					<td>&euro; {$aData.Profile.bedrg5}</td>
				</tr>
			{/if}
			{if $aData.Profile.artoms6 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms6}</td>
					<td>{$aData.Profile.aantal6}</td>
					<td>&euro; {$aData.Profile.bedrg6}</td>
				</tr>
			{/if}

			{if $aData.Profile.artoms7 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms7}</td>
					<td>{$aData.Profile.aantal7}</td>
					<td>&euro; {$aData.Profile.bedrg7}</td>
				</tr>
			{/if}
			{if $aData.Profile.artoms8 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms8}</td>
					<td>{$aData.Profile.aantal8}</td>
					<td>&euro; {$aData.Profile.bedrg8}</td>
				</tr>
			{/if}
			{if $aData.Profile.artoms9 > ""}
				<tr class="">
					<td>{$aData.Profile.artoms9}</td>
					<td>{$aData.Profile.aantal9}</td>
					<td>&euro; {$aData.Profile.bedrg9}</td>
				</tr>
			{/if}
      <tr class="totalBetalen">
      	<td></td>
        <td colspan="1" valign="top" >
            Totaal te betalen 
        </td>
        <td  align="right" valign="top">
           &euro;{$aData.Profile.inclwaarde|replace:'.':','} 
        </td>
      </tr>

    
    </tbody>
  </table>
</div>

<div class="body-bottom col-md-12">
	<div>
      De betalingen zijn tot en met {$aData.Profile.bet_verw|date_format:"%e %B %Y"} verwerkt. Heeft u inmiddels betaald, dan hebben uw betaling en deze herinnering elkaar gekruist. In dat geval kunt u deze herinnering als niet ontvangen beschouwen.
  </div>

  <div>Heeft u nog vragen?</div> 
   <p>	
	  	Mocht u vragen of opmerkingen hebben, neem dan contact op met de afdeling Service & Advies van de Consumentenbond, via <a href="https://www.consumentenbond.nl/service/contactformulier">https://www.consumentenbond.nl/service/contactformulier</a>. Zij zijn ook telefonisch bereikbaar op 070 - 445 45 45 tijdens werkdagen tussen 08.30 en 18.00 uur. Of kijk eens op onze website voor <a class="" style="font-family: Arial, Helvetica, 'Helvetica Neue', sans-serif; font-size: 16px; font-weight: normal; font-style: normal; text-decoration: underline; color: #0072bc;" title="" href="https://www.consumentenbond.nl/service/betalen" target="_blank" data-conversionlink="true">meer informatie over betalen</a>.
  </p>

</div>
<div class="footer col-md-12">

	<div class="footer-links col-md-12">
	
	  <a href="http://consumentenbond.nl/over-ons/voorwaarden-en-privacy/privacy/privacyverklaring/#anchor_3" target="_blank">
	  	Privacy
	  </a>
		<span class="mHide">&nbsp;&nbsp;-&nbsp;&nbsp;</span>

	  <a href="http://consumentenbond.nl/over/voorwaarden/Disclaimer/" target="_blank">
	  	Disclaimer
	  </a>
	  <span class="mHide">&nbsp;&nbsp;-&nbsp;&nbsp;</span>

	  <span >Uw&nbsp;e-mailadres: {$aData.Profile.email} <a href=""></a></span>
	</div>

	<div class="col-md-12">
		U ontvangt dit bericht omdat u een lidmaatschap heeft bij&nbsp;de Consumentenbond.&nbsp;<br>

    © Consumentenbond {$smarty.now|date_format:"Y"}
    <br>
    BTW-nummer:&nbsp;NL0028.77.326.B.01 &nbsp; &nbsp; &nbsp; &nbsp;BIC code: ABNANL2A &nbsp; &nbsp; &nbsp; &nbsp;KVK: 40 40 94 95
	</div>
</div>

