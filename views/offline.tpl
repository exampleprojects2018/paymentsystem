<div id="offline">
<!-- 	<h2>Offline</h2> -->
	<hr>
	<p>Geachte heer/mevrouw,</p>
	<br>
	<p>	U heeft zojuist gebruik willen maken van de iDEAL betaalmogelijkheid. Helaas is deze functionaliteit voor korte tijd niet toegankelijk omdat wij de betaaloplossing aan het vernieuwen zijn. We verwachten dit binnen enkele uren weer operationeel te hebben. Probeert u het straks nogmaals a.u.b. Excuses voor het ongemak.
	</p>
	<br>
	<p class="small">		
			Met vriendelijke groet,
			<br>
			Marilyn Bonnet
			<br>
			Teamleider Financiële administratie
	</p>
	<br>
	<img src="{$smarty.const.ASSETS}/consumentenbond_logo.png" alt="Consumentenbond" width="200">
	<br><br>
	<hr>
</div>