<!DOCTYPE html>

<html>

    <head>

        <title>Consumentenbond</title>

        <meta charset="UTF-8">
        <!-- Mobile View -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

        <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300italic,300,700,700italic' rel='stylesheet' type='text/css'>

        <link href="<?php echo WEB_ROOT; ?>/css/main.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEB_ROOT; ?>/css/datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEB_ROOT; ?>/css/media-screens.css" rel="stylesheet" type="text/css">


        <!-- BOOTSTRAP -->
        <script src="https://code.jquery.com/jquery-2.2.4.js"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

        <!-- JS files -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="<?php echo WEB_ROOT; ?>/js/conf.js"></script>
        <script src="<?php echo WEB_ROOT; ?>/js/main.js"></script>
        <script src="<?php echo WEB_ROOT; ?>/js/getAddress.js"></script>
        <script src="<?php echo WEB_ROOT; ?>/js/datepicker.js"></script>


    </head>

    <body >

        <div class="content">